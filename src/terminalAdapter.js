const Configurapi = require('configurapi');
const parser = require('yargs-parser');

function split(str) 
{
    let spacePosition = str.indexOf(' ');
    
    return (spacePosition === -1) ? [str] : [str.substr(0, spacePosition), str.substr(spacePosition).trim()];
};

module.exports = {
    write: function(response)
    {
        console.log(response)
    },

    toRequest: function(line)
    {
        let request = new Configurapi.Request();
        
        let tokens = split(line);

        request.name = tokens[0];
        request.method = '';
        request.params = tokens.length > 1 ? parser(tokens[1]) : {};
        delete request.params['_'];
        request.payload = undefined;
        request.query = {}
        request.path = '';
        request.pathAndQuery = '';

        request.headers = {};

        return request;
    }
};
