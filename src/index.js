const events = require('events');
const Configurapi = require('configurapi');
const readline = require('readline');
const TerminalAdapter = require('./terminalAdapter');

module.exports = class TerminalRunner extends events.EventEmitter {
    constructor() {
        super();

        this.configPath = "config.yaml";
        this.service = undefined;
        this.terminal = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }

    run(options) {
        this._handleOptions(options);

        let config = Configurapi.Config.load(this.configPath);

        this.service = new Configurapi.Service(config);
        this.service.on("trace", (s) => this.emit("trace", s));
        this.service.on("debug", (s) => this.emit("debug", s));
        this.service.on("error", (s) => this.emit("error", s));

        this.emit("trace", "Server is running...");
        
        this.terminal.on('SIGINT', () => {
            process.exit();
        });

        this._process()
    }

    _handleOptions(options) {
        if (!options) return;

        if ('configPath' in options) this.configPath = options.configPath;
    }

    async _process(back) {
        await this.terminal.question('> ', async (answer)=>
        {
            try
            {
                answer = answer.trim();
                let event = new Configurapi.Event(TerminalAdapter.toRequest(answer));
                event.params = event.request.params;
                await this.service.process(event);

                TerminalAdapter.write(event.response);
            }
            catch(error)
            {
                let response = new Configurapi.ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);
                TerminalAdapter.write(response);
            }

            this._process();
        });
    }
};
