#!/usr/bin/env node

const commander = require('commander');
const App = require('./index');
const oneliner = require('one-liner');

///////////////////////////////////
// Utility functions
///////////////////////////////////
function format(prefix, str)
{
    let time = new Date().toISOString();
    return `${time} ${prefix}: ${oneliner(str)}`;
}
function logTrace(s)
{
    console.log(format('Trace', s));
}
function logError(s)
{
    console.error(format('Error', s));
}
function logDebug(s)
{
    console.log(format('Debug', s));
}

///////////////////////////////////
// Process arguments
///////////////////////////////////
commander.option('-f, --file [path]', 'Path to the config file')
         .parse(process.argv);

let configPath = commander.file ? commander.file : 'config.yaml';

///////////////////////////////////
// Start the API
///////////////////////////////////
let app = new App();
app.on('error', (s) => {logError(s);});
app.on('debug', (s) => {logDebug(s);});
app.on('trace', (s) => {logTrace(s);});
app.run({configPath:configPath});